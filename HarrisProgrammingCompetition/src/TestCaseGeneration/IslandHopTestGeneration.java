package TestCaseGeneration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Random;

public class IslandHopTestGeneration
{
   // Generate test case with 1000 islands
   final static int NUM_ISLANDS = 75;
   final static int MAX_BUDGET = 500;
   final static int TOLL_MAX = 99;
   final static int TOLL_MIN = 1;
   final static int DISTANCE_MAX = 1000;
   final static int DISTANCE_MIN = 1;
   final static int NUM_TESTS = 1000;
   final static int SCALE = 100;

   // tolls[x][y] == toll of bridge from island x to island y
   // tolls[x][y] == tolls[y][x]
   // tolls[x][x] == 0
   static int[][] tolls = new int[NUM_ISLANDS][NUM_ISLANDS];

   // distances[x][y] == distance of bridge from island x to island y
   // distances[x][y] == distances[y][x]
   // distances[x][x] == 0
   static int[][] distances = new int[NUM_ISLANDS][NUM_ISLANDS];

   public static void main(String[] args) throws FileNotFoundException
   {
      // Use prng to generate all values for array
      Random gen = new Random();

      for (int i = 0; i < NUM_ISLANDS; i++)
      {
         for (int j = 0; j < i; j++)
         {
            distances[i][j] = gen.nextInt(DISTANCE_MAX);
            distances[j][i] = distances[i][j];

            tolls[i][j] = gen.nextInt(TOLL_MAX) + TOLL_MIN;
            tolls[j][i] = tolls[i][j];
         }
      }

      StringBuilder out = new StringBuilder();
      out.append(NUM_ISLANDS + "\n");
      out.append(NUM_TESTS + "\n");
      for (int i = 0; i < NUM_ISLANDS; i++)
      {
         for (int j = 0; j < NUM_ISLANDS; j++)
         {
            out.append(tolls[i][j]);
            out.append(" ");
         }
         out.append("\n");
      }

      for (int i = 0; i < NUM_ISLANDS; i++)
      {
         for (int j = 0; j < NUM_ISLANDS; j++)
         {
            out.append(distances[i][j]);
            out.append(" ");
         }
         out.append("\n");
      }

      // Create list of starting island, ending island, budgets
      for (int i = 0; i < NUM_TESTS; i++)
      {
         int start = gen.nextInt(NUM_ISLANDS) + 1;
         int fin = gen.nextInt(NUM_ISLANDS) + 1;
         while (fin == start)
         {
            fin = gen.nextInt(NUM_ISLANDS) + 1;
            if (fin != start)
            {
               break;
            }
         }
         out.append(start);
         out.append(" ");

         out.append(fin);
         out.append(" ");
         out.append(gen.nextInt(MAX_BUDGET));
         out.append("\n");
      }

      PrintStream output = new PrintStream(new FileOutputStream(new File("src\\Input\\sampleislandhop.txt")));
      System.setOut(output);
      System.out.print(out);
      output.close();
   }
}
