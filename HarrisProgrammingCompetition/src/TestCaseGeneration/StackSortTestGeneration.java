package TestCaseGeneration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class StackSortTestGeneration
{
    // Don't hard code constants
    final static int TESTS = 250;
    final static int MAX = 100;

    public static void main(String[] args) throws FileNotFoundException
    {

        Random gen = new Random();
        StringBuilder out = new StringBuilder();

        // Output number of tests
        out.append(TESTS + "\n");

        // Randomly generate values for each item
        for (int i = 0; i < TESTS; i++)
        {
            // Random between 3 and 100
            int n = gen.nextInt(MAX - 2) + 3;
            
            out.append(n + "\n");

            // Create array of 1, ..., n
            ArrayList<Integer> arr = new ArrayList<Integer>();
            for (int j = 1; j <= n; j++)
            {
                arr.add(j);
            }
            System.out.println(arr);
            
            Collections.shuffle(arr); 
            
            System.out.println(arr);
            
            for(int j = 0; j < arr.size(); j++)
            {
                out.append(arr.get(j) + " ");
            }
            out.append("\n");
        }

        PrintStream output = new PrintStream(new FileOutputStream(new File("src\\Input\\StackSort.txt")));
        System.setOut(output);
        System.out.print(out);
        output.close();
    }
}
