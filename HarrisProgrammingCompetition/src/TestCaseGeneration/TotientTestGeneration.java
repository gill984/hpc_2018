package TestCaseGeneration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Random;

public class TotientTestGeneration
{
    // Don't hard code constants
    final static int TESTS = 5000;
    final static int MAX = 100000000;

    public static void main(String[] args) throws FileNotFoundException
    {

        Random gen = new Random();
        StringBuilder out = new StringBuilder();
        
        // Output number of tests
        out.append(TESTS + "\n");

        // Randomly generate values for each item
        for (int i = 0; i < TESTS; i++)
        {
            int val = gen.nextInt(MAX - 2) + 2;
            out.append(val);
            out.append("\n");
        }

        PrintStream output = new PrintStream(new FileOutputStream(new File("src\\Input\\Totient.txt")));
        System.setOut(output);
        System.out.print(out);
        output.close();
    }
}
