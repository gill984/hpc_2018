package TestCaseGeneration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Random;

public class StockingUpTestGeneration
{
    // Don't hard code constants
    final static int NUM_ITEMS = 10;
    final static int CAPACITY = 25;
    final static int BUDGET = 15;
    final static int MAX_SIZE = 10;
    final static int MAX_VALUE = 10;
    final static int MAX_COST = 5;

    public static void main(String[] args) throws FileNotFoundException
    {

        Random gen = new Random();
        StringBuilder out = new StringBuilder();

        out.append(NUM_ITEMS + "\n");
        out.append(CAPACITY + "\n");
        out.append(BUDGET + "\n");

        // Randomly generate values for each item
        for (int i = 0; i < NUM_ITEMS; i++)
        {
            out.append(gen.nextInt(MAX_SIZE) + 1);
            out.append(" ");
            out.append(gen.nextInt(MAX_COST) + 1);
            out.append(" ");
            out.append(gen.nextInt(MAX_VALUE) + 1);
            out.append("\n");
        }

        // Print this out for debugging and also write it to the Input directory
        System.out.println(out);
        PrintStream output = new PrintStream(new FileOutputStream(new File("src\\Input\\sampleStockingUp.txt")));
        System.setOut(output);
        System.out.print(out);
        output.close();
    }
}
