package Solutions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.Stack;

class StackSort
{
    public static void main(String[] args) throws Exception
    {
        Scanner in = new Scanner(new FileInputStream("src\\Input\\StackSort.txt"));
        StringBuilder out = new StringBuilder();
        int cases = in.nextInt();

        // For each test case
        while (cases-- > 0)
        {
            int n = in.nextInt();
            int[] arr = new int[n];

            for (int i = 0; i < n; i++)
            {
                arr[i] = in.nextInt();
            }

            for (int i = 0; i <= n; i++)
            {
                if (isSorted(arr))
                {
                    out.append(i + "\n");
                    break;
                }
                arr = stackSort(arr);

            }
        }

        System.out.println(out);
        PrintStream output = new PrintStream(new FileOutputStream(new File("src\\Output\\StackSort_output.txt")));
        System.setOut(output);
        System.out.print(out);
        output.close();
        in.close();
    }

    public static int[] stackSort(int[] arr)
    {
        int[] ret = new int[arr.length];
        Stack<Integer> stack = new Stack<Integer>();

        int ret_idx = 0;
        for (int i = 0; i < arr.length; i++)
        {
            int p = arr[i];

            if (stack.isEmpty())
            {
                stack.push(p);
            } else
            {

                while (!stack.isEmpty())
                {
                    
                    if(p > stack.peek())
                    {
                        ret[ret_idx] = stack.pop();
                        ret_idx += 1;
                    }
                    else
                    {
                        break;
                    }
                }
                
                // At this point the stack is either empty or p < stack.peekLast()
                // Go ahead and put p on the stack
                stack.push(p);
            }
        }

        while (!stack.isEmpty())
        {
            ret[ret_idx] = stack.pop();
            ret_idx += 1;
        }
        
        return ret;
    }

    public static boolean isSorted(int[] arr)
    {
        for (int i = 0; i < arr.length - 1; i++)
        {
            if (arr[i] > arr[i + 1])
            {
                return false;
            }
        }
        return true;
    }
}