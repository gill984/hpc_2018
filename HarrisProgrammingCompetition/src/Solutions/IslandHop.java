package Solutions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Scanner;

public class IslandHop
{
   public static void main(String[] args) throws Exception
   {
      Scanner in = new Scanner(new FileInputStream("src\\Input\\islandhop.txt"));
      StringBuilder out = new StringBuilder();

      int numNodes = in.nextInt();
      int tests = in.nextInt();

      int[][] distances = new int[numNodes][numNodes];
      int[][] tolls = new int[numNodes][numNodes];

      // 2 because there are distances and tolls
      for (int k = 0; k < 2; k++)
      {
         for (int i = 0; i < numNodes; i++)
         {
            for (int j = 0; j < numNodes; j++)
            {
               if (k == 0)
               {
                  tolls[i][j] = in.nextInt();
               }
               else
               {
                  distances[i][j] = in.nextInt();
               }
            }
         }
      }

      for (int i = 0; i < tests; i++)
      {
         // The node this mission starts at
         int start = in.nextInt();

         // The node this mission ends at
         int goal = in.nextInt();

         // The budget for this mission
         int budget = in.nextInt();

         // PriorityQueue of paths, this is a minheap of paths arranged by
         // distance
         PriorityQueue<Path> paths = new PriorityQueue<Path>();

         // Map from Node number to a list of best paths which point to this
         // node
         HashMap<Integer, ArrayList<Path>> goalOptimals = new HashMap<Integer, ArrayList<Path>>();

         // Algorithm base case. It 0 cost and distance to the start node
         paths.offer(new Path(start - 1, 0, 0));

         // Algorithm terminates when this variable is set to true
         boolean found = false;

         while (!paths.isEmpty())
         {
            Path p = paths.poll();
            if (p.current == goal - 1)
            {
               out.append(p.distance + " ");

               found = true;
               break;
            }

            // For each neighbor of this node
            for (int j = 0; j < numNodes; j++)
            {
               if (j == p.current)
               {
                  // don't add the same path to the queue
                  continue;
               }

               if (p.toll + tolls[p.current][j] <= budget)
               {
                  /*
                   * Normally in Dijkstra's you would check a visited list to
                   * see if the path should be added. However, because the
                   * amount of money needed may not be enough along the shortest
                   * path, we can't throw out paths which are longer in
                   * distance. We could however throw out a path which is longer
                   * in distance and costs more money than any path discovered
                   * so far. In other words a path has to have more cost and
                   * more distance for it to be disregarded.
                   */
                  boolean addPath = true;
                  ArrayList<Path> optimals = goalOptimals.get(j);

                  // If the Map has no entry for this value yet, create a new
                  // one
                  if (optimals == null)
                  {
                     goalOptimals.put(j, new ArrayList<Path>());
                     optimals = goalOptimals.get(j);
                  }

                  // For all the best paths to this particular node
                  for (Path optimal : optimals)
                  {
                     if (optimal.toll < p.toll + tolls[p.current][j]
                         && optimal.distance < p.distance + distances[p.current][j])
                     {
                        addPath = false;
                        break;
                     }
                  }

                  // If addpath is still true, we want to investigate this path
                  // and all of it's neighbor paths. So put it on the queue and
                  // into the list of optimals.
                  if (addPath)
                  {
                     Path nextPath = new Path(j,
                                              p.distance + distances[p.current][j],
                                              p.toll + tolls[p.current][j]);
                     paths.offer(nextPath);
                     optimals.add(nextPath);
                  }
               }
            }
         }

         if (!found)
         {
            out.append("-1 ");
         }
      }
      System.out.print(out);

      PrintStream output = new PrintStream(new FileOutputStream(new File("src\\output\\islandhop_output.txt")));
      System.setOut(output);
      System.out.print(out);
      output.close();
      in.close();
   }
}

// Custom class which represents a path
class Path implements Comparable<Object>
{
   public int current;
   public int distance;
   public int toll;

   public Path(int c, int d, int t)
   {
      this.current = c;
      this.distance = d;
      this.toll = t;
   }

   // Implement this method in order to have Java's priorityqueue keep things
   // arranged in the correct order
   @Override
   public int compareTo(Object o)
   {
      return (this.distance - ((Path) o).distance);
   }

   @Override
   public String toString()
   {
      return ("Current node: " + current + ", distance: " + distance + ", toll: " + toll);
   }
}