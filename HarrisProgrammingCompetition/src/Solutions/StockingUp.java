package Solutions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class StockingUp
{
   // Don't hard code constants
   private static final int SIZE_IDX = 0;
   private static final int COST_IDX = 1;
   private static final int VAL_IDX = 2;
   private static final int ARR_COLS = 3;

   public static void main(String[] args) throws Exception
   {
      // Read input from generated test input
      Scanner in = new Scanner(new FileInputStream("src\\Input\\StockingUp.txt"));

      // Build output string as problem is solved
      StringBuilder out = new StringBuilder();

      // Read in number of items, total capacity, and total budget
      int n = in.nextInt();
      int capacity = in.nextInt();
      int budget = in.nextInt();

      // Read input up front into an array in memory
      int[][] size_cost_value = new int[n][ARR_COLS];
      for (int i = 0; i < n; i++)
      {
         size_cost_value[i][SIZE_IDX] = in.nextInt();
         size_cost_value[i][COST_IDX] = in.nextInt();
         size_cost_value[i][VAL_IDX] = in.nextInt();
      }

      // Initialize dp array
      // dp[i][j] = max value for i cost and j size
      int[][] dp = new int[budget + 1][capacity + 1];
      for (int i = 0; i < dp.length; i++)
         for (int j = 0; j < dp[i].length; j++)
            dp[i][j] = -1;

      /*
       * Algorithm: This is a standard knapsack problem but executed in 2D. A 2D
       * dp array is created where rows represent cost and columns represent
       * size. So dp[c][s] is equal to the maximum value attainable with exactly
       * c cost and s size used. The base case is that dp[0][0] is equal to 0
       * because for 0 cost and 0 size used we can have 0 value by picking no
       * items. Next, for each item, iterate backwards through dp. For each
       * configuration found (not equal to -1), try adding this new item to it.
       * The item can be added if the cost and size of the item stay within
       * bounds of the dp array when added to the current indexes we are at in
       * dp. Whether the element we reach by adding the cost and size to the
       * current index is replaced is determined by whether this new
       * configuration or the existing one has a higher value. See line dp[c +
       * cost][s + size] = Math.max(dp[c][s] + value, dp[c + cost][s + size]).
       * The max variable is updated similarly to avoid iterating through the dp
       * array after finishing the algorithm.
       */
      dp[0][0] = 0;
      int max = 0;

      for (int i = 0; i < n; i++)
      {
         int size = size_cost_value[i][SIZE_IDX];
         int cost = size_cost_value[i][COST_IDX];
         int value = size_cost_value[i][VAL_IDX];

         for (int c = budget; c >= 0; c--)
         {
            for (int s = capacity; s >= 0; s--)
            {
               // Check if this is a valid configuration and if we will be in
               // bounds when
               // adding to indexes
               if (dp[c][s] != -1 && cost + c < dp.length && s + size < dp[c].length)
               {
                  // If this is a more valuable configuration, update dp and max
                  dp[c + cost][s + size] = Math.max(dp[c][s] + value, dp[c + cost][s + size]);
                  max = Math.max(max, dp[c][s] + value);
               }
            }
         }
      }

      out.append(max);
      System.out.print(out.toString());

      PrintStream output = new PrintStream(new FileOutputStream(new File("src\\output\\StockingUp_Output.txt")));
      System.setOut(output);
      System.out.print(out);
      output.close();
      in.close();
   }
}