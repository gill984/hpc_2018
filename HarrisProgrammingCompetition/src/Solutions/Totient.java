package Solutions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

class Totient
{
    public static void main(String[] args) throws Exception
    {
        /*
         * Algorithm: Use Euler's product formula
         * 
         * totient(n) = totient(p_1 ^ k_1) * ...
         * 
         * totient(p_r ^ k_r) = n * (1 - 1/p_1) * ... * (1 - 1/p_r).
         * 
         * See the wikipedia article on totient for more information.
         * 
         * The first step is calculating all the primes under sqrt(1000000). Next,
         * iterate over the list of primes and for each prime which evenly divides n,
         * 
         * primes[i] is a prime divisor of n. Continue to divide n until it is the
         * smallest possible number divisible by this prime once it is a number
         * indivisible by n, continue, the number you are left with will be one or it
         * will be a prime larger than 1000 we don't actually need to keep track of the
         * number of times we divide
         */

        // This array is a siev. For an index, if the value stored at that index is
        // false, then this index is prime, if true then the value is not prime. In Java
        // booleans are initialized to false.
        boolean[] siev = new boolean[100000];
        siev[0] = true;
        siev[1] = true;

        // Array List to store primes
        ArrayList<Integer> primes = new ArrayList<Integer>();

        // Populate the siev. When a prime is reached, mark all of it's multiples as
        // non-prime. Also populate the primes list.
        for (int i = 2; i < siev.length; i++)
        {
            if (!siev[i])
            {
                primes.add(i);
                for (int j = i + i; j < siev.length; j += i)
                {
                    siev[j] = true;
                }
            }
        }

        Scanner in = new Scanner(new FileInputStream("src\\Input\\Totient.txt"));
        StringBuilder out = new StringBuilder();
        int cases = in.nextInt();

        // For each test case
        while (cases-- > 0)
        {
            int initial = in.nextInt();
            int n = initial;
            int totient = initial;

            for (int i = 0; i < primes.size(); i++)
            {
                if (n % primes.get(i) == 0)
                {
                    while (n % primes.get(i) == 0)
                    {
                        n /= primes.get(i);
                    }
                    totient = totient - totient / primes.get(i);
                }
            }

            if (n != 1)
            {
                totient = totient - totient / n;
            }
            out.append(totient + "\n");
        }

        System.out.println(out.toString());
        PrintStream output = new PrintStream(new FileOutputStream(new File("src\\Output\\TotientOutput.txt")));
        System.setOut(output);
        System.out.print(out);
        output.close();
        in.close();
    }
}