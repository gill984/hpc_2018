package Solutions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.PriorityQueue;
import java.util.Scanner;

public class GreedyStockingUp
{
   // Don't hard code constants
   private static final int SIZE_IDX = 0;
   private static final int COST_IDX = 1;
   private static final int VAL_IDX = 2;
   private static final int ARR_COLS = 3;

   public static void main(String[] args) throws Exception
   {
      // Read input from generated test input
      Scanner in = new Scanner(new FileInputStream("src\\Input\\StockingUp.txt"));

      // Build output string as problem is solved
      StringBuilder out = new StringBuilder();

      // Read in number of items, total capacity, and total budget
      int n = in.nextInt();
      int capacity = in.nextInt();
      int budget = in.nextInt();

      // Read input up front into array in memory
      int[][] size_cost_value = new int[n][ARR_COLS];
      for (int i = 0; i < n; i++)
      {
         size_cost_value[i][SIZE_IDX] = in.nextInt();
         size_cost_value[i][COST_IDX] = in.nextInt();
         size_cost_value[i][VAL_IDX] = in.nextInt();
      }

      // Create a priorityqueue which looks at value
      // Heuristic = value / (size/2 + cost)
      PriorityQueue<Item> q = new PriorityQueue<Item>();
      for (int i = 0; i < size_cost_value.length; i++)
      {
         q.offer(new Item(size_cost_value[i][SIZE_IDX],
                          size_cost_value[i][COST_IDX],
                          size_cost_value[i][VAL_IDX]));
      }

      int total_cost = 0;
      int total_size = 0;
      int total_val = 0;

      // Until the queue is empty, attempt every item in order of the heuristic
      while (!q.isEmpty())
      {
         Item next = q.poll();

         System.out.println(next.h);

         if (total_cost + next.cost <= budget && total_size + next.size <= capacity)
         {
            total_cost += next.cost;
            total_size += next.size;
            total_val += next.value;
         }
      }

      out.append(total_val);

      System.out.print(out.toString());

      PrintStream output = new PrintStream(new FileOutputStream(new File("src\\output\\StockingUp_Output_Greedy.txt")));
      System.setOut(output);
      System.out.print(out);
      output.close();
      in.close();
   }
}

class Item implements Comparable<Item>
{
   public int size;
   public int cost;
   public int value;
   public double h;

   public Item(int s, int c, int v)
   {
      this.size = s;
      this.cost = c;
      this.value = v;

      this.h = (double) (1000 * value) / (size * size + cost * cost);
   }

   @Override
   public int compareTo(Item d)
   {
      return (int) (d.h - this.h);
   }
}